Configuration settings needed for LiveReload to work locally
without a browser plugin or extra code in your .tpl file (that
can mistakenly be forgotten when pushed to production).

Recommended to use along side the Gruntjs grunt-contrib-watch npm.
https://www.npmjs.org/package/grunt-contrib-watch


Directions

Move, or copy, the "livereload_insert" directory into the Drupal
modules directory, `sites/all/modules` and enable.

Add the following to the Drupal settings file.
`sites/default/settings.php`

// LiveReload Insert - set the URL path of your local environment.
$conf['livereload_insert_local_path'] = 'local.example'; // leave off "http://" or "https://"

// LiveReload Insert - set LiveReload port. Default port is 35729.
$conf['livereload_insert_local_port'] = '3001';
